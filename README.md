# Erazem's F-Droid repository

This is an [F-Droid](https://f-droid.org/) repository for applications not available
in the official F-Droid repositories. The setup process was inspired by
[Ricki's fdroid-firefox](https://gitlab.com/rfc2822/fdroid-firefox/).

Updates to the applications are checked for every day using
[Gitlab pipeline schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).

The build process uses a [custom container image](https://gitlab.com/erazemk/fdroidserver),
based on the upstream F-Droid [ci-images-base](https://gitlab.com/fdroid/ci-images-base) image.

To add the repo to F-Droid add the following url to the *Repositories* section of the app's settings:
[https://fdroid.erazem.eu/fdroid/repo](https://fdroid.erazem.eu/fdroid/repo?fingerprint=92C73B27F68A38B2365AA526E979E84F5C713971B0B8271FC26FE0FBA4E0B150)

# Set up your own repository

If you'd like to create your own repository, similar to this one, it's fairly easy:

1. Clone/Fork the project
1. Edit `fdroid/config.yml` to your liking (at least the `repo_url` and `keydname`)
1. Edit files in `fdroid/metadata` for your apps
1. Edit `index.html` to describe your repo
1. Add custom pipeline variables under *Settings -> CI/CD -> Variables* [^1]
1. (Optional) Update/Copy `updates.sh` to properly check for updates for your applications
1. (Optional) Edit `.gitlab-ci.yml` to use a custom container image
1. (Optional) Add a pipeline schedule to automatically check for updates

# License

The project is licensed under the [MIT license](LICENSE).

---
[^1]: `keypass` is the certificate/keystore password that's used in `fdroid/config.yml`,
`keystore` is the base64 encoded keystore that's decoded into `keystore.p12`.
You can use `keytool` to generate both or use `fdroid init -v` to generate them with
F-Droid's built in tools - this will create a new `config.yml` file, from which you should
copy the `keystorepass/keypass` (they will be the same) and the `keystore.p12` file, which
you should base64 encode. Then you should add both as pipeline variables.
