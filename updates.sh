#!/bin/sh
# Checks if any app updates are available and downloads them if there are

set -o noclobber
set -o errexit
set -o nounset

signal() {
    echo "--- Checking for Signal updates ---"

    # Download Signal's version info json file and parse it for info
    curl -so signal.json https://updates.signal.org/android/latest.json
    LATEST_VERSION="$(jq -r '.versionName' signal.json)"
    LATEST_APK_NAME="$(jq -r '.url' signal.json | cut -d'/' -f5)"
    echo "Latest version: ${LATEST_VERSION}"

    # Parses the F-Droid index-v1.json file with jq to get the 'versionName' variable
    CURRENT_VERSION="$(curl -s https://fdroid.erazem.eu/fdroid/repo/index-v1.json \
            | jq -r '.packages."org.thoughtcrime.securesms"[0].versionName')"
    echo "Current version in repo: ${CURRENT_VERSION}"

    # Removes '.' from version numbers and compares them as normal numbers
    if [ "$(echo "$LATEST_VERSION" | tr -d '.')" -gt "$(echo "$CURRENT_VERSION" | tr -d '.')" ]; then
        # Outdated - download new version
        echo "Outdated, downloading new version"
        curl -so fdroid/repo/"${LATEST_APK_NAME}" "$(jq -r '.url' signal.json)"
    else
        # No new updates - don't do anything
        echo "Up to date"

        # TODO: Currently if the application is up-to-date, the original apk gets deleted when a new
        # page is deployed, so as a dirty hack we have to still download the apk every day, even if
        # it's the latest version
        curl -so fdroid/repo/"${LATEST_APK_NAME}" "$(jq -r '.url' signal.json)"
    fi

    echo "-----------------------------------"
}

# Check for updates for applications listed below
signal
